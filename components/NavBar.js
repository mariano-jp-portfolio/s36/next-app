import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import Link from 'next/link';

import UserContext from '../UserContext';


export default function NavBar() {
    //consume the UserContext and destructure it to access the user state from context provider
    
    const { user } = useContext(UserContext);
    // console.log(user.email);
    // console.log(user.isAdmin);

    return (
        <Navbar bg="dark" variant="dark" expand='lg'>
            <Link href="/">
                <a className="navbar-brand">Course Booking</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse>
                <Nav className="mr-auto">
                    <Link href="/courses">
                        <a className="nav-link" role="button">Courses</a>
                    </Link>

                    {(user.id !== null)
                        ? (user.isAdmin === true)
                         ? 
                            <React.Fragment>
                                <Link href="#">
                                    <a className="nav-link" role="button">Add Course</a>
                                </Link>
                               {/* <Link href="/register">
                                    <a className="nav-link" role="button">Register</a>
                                </Link>*/}
                                <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                                </Link>
                            </React.Fragment>
                            :
                            <Link href="/logout">
                                    <a className="nav-link" role="button">Logout</a>
                            </Link>
                        :
                        <React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
   
}
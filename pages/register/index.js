import { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { useRouter } from 'next/router';

export default function index() {
	// router
	const router = useRouter();
	
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState(''); 
	const [isActive, setIsActive] = useState(false);

   function registerUser(e) {
		e.preventDefault();
		
		// fetch().then(for res).then(for data manipulation)
		fetch('http://localhost:4000/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json()) // response first
		.then(data => { // manipulation of data
			// console.log(data);
			// if no dupe email, proceed with the registration
			if (data === false) {
				fetch('http://localhost:4000/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1					
					})
				})
				.then(res => res.json())
				.then(data => {
					// console.log(data);
					if (data) {
						setEmail('');
						setPassword1('');
						setPassword2('');
						setFirstName('');
						setLastName('');
						setMobileNo('');
						
						alert('Thank you for registering with us!');
						
						router.push('/login');
					}
				});
			}
		});
	}

	useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
			},[email, password1, password2]);

		return (
			<Container>
				<h2 className="text-center">Register</h2>
				<Form onSubmit={e => registerUser(e)} className="col-lg-4 offset-lg-4 my-5">
				
						<Form.Group>
							<Form.Label>First Name</Form.Label>
							<Form.Control 
								type="text"
								placeholder="First name"
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								required
							/>
						</Form.Group>
						
						<Form.Group>
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
								type="text"
								placeholder="Last name"
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								required
							/>
						</Form.Group>
						
						<Form.Group>
							<Form.Label>Mobile No.</Form.Label>
							<Form.Control 
								type="text"
								placeholder="Mobile number"
								value={mobileNo}
								onChange={e => setMobileNo(e.target.value)}
								required
							/>
						</Form.Group>
						
						<Form.Group>				
						<Form.Label>Email Address</Form.Label>
						<Form.Control 
							type="email"
							placeholder="Enter email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Password"
							value={password1}
							onChange={e => setPassword1(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group>
						<Form.Label>Verify Password</Form.Label>
						<Form.Control 
							type="password"
							placeholder="Verify Password"
							value={password2}
							onChange={e => setPassword2(e.target.value)}
							required
						/>
					</Form.Group>
					<Form.Text className="text-muted">
							We'll never share your deets with anyone else.
					</Form.Text>
					
					<br />

					{isActive
						?
						<Button 
							variant="primary"
							type="submit"
							id="submitBtn"
						>
							Submit
						</Button>
						:
						<Button 
							variant="secondary"
							type="submit"
							id="submitBtn"
							disabled
						>
							Submit
						</Button>
					}
				</Form>
			</Container>
		)
};


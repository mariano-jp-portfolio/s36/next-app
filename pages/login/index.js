import { useEffect, useState, useContext } from 'react';
import { Row, Col, Container, Form, Button } from 'react-bootstrap';

import UserContext from '../../UserContext';
import View from '../../components/View';

import Router from 'next/router';

import AppHelper from '../../app-helper';

// Google login
import { GoogleLogin } from 'react-google-login';
// SweetAlert
import Swal from 'sweetalert2';

export default function index() {
	return (
		<View title={ 'Login' }>
			<Row className="justify-content-center">
				<Col xs md="6">
					<LoginForm />
				</Col>
			</Row>
		</View>
	);
};

const LoginForm = () => {
	const { setUser } = useContext(UserContext);
	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);
	
	function authenticate(e) {
		e.preventDefault();
		
		// fetch requires the endpoint (url) & an object {method, headers, body}
		fetch(`${AppHelper.API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data);
			// if successful login, a token will be generated, save it in the user's localStorage
			// localStorage.setItem('token', data.accessToken);
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);		

				setEmail('');
		    	setPassword('');
			} else {
				if (data.error === 'does-not-exist') {
					Swal.fire('Authentication Failed', 'User does not exist.', 'error');
				} else if (data.error === 'incorrect-password') {
					Swal.fire('Authentication Failed', 'Please check your password.', 'error');
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login procedure.', 'error');
				}
			}
		});
	}
	
	// GOOGLE LOGIN
	const authenticateGoogleToken = (response) => {
		// console.log(response);
		const options = {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				tokenId: response.tokenId
			})
		}
		fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			// console.log(typeof data.accessToken);
			if (typeof data.accessToken !== 'undefined') {
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
			} else {
				if (data.error === 'google-auth-error') {
					Swal.fire('Google Auth Error', 'Google authentication procedure failed.', 'error');
				} else if (data.error === 'login-type-error') {
					Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login procedure.', 'error');
				}
			}
		});
	};
	
	// EMAIL LOGIN
	const retrieveUserDetails = (accessToken) => {
		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}
		
		fetch(`${AppHelper.API_URL}/users/details`, options)
		.then(AppHelper.toJSON)
		.then(data => {
			// console.log(data);
			
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
			
			// welcome alert
			Swal.fire('Welcome Back', `How's your day, ${data.firstName}?`);
			
			// router
			Router.push('/courses');
		});
	};
	
	useEffect(() => {
		if (email !== '' && password !== '') {
			setIsActive(true);
		}
	}, [email, password]);
	
	return (
		<Container>
			<h2>Login</h2>
		    <Form onSubmit={ authenticate }>
		        <Form.Group controlId="userEmail">
	                <Form.Label>Email address</Form.Label>
	                <Form.Control 
	                    type="email" 
	                    placeholder="Enter email" 
	                    value={email}
	                    onChange={(e) => setEmail(e.target.value)}
	                    required
	                />
		        </Form.Group>

		        <Form.Group controlId="password">
		            <Form.Label>Password</Form.Label>
			        <Form.Control 
	                  type="password"
	                  placeholder="Password" 
			            value={password}
			            onChange={(e) => setPassword(e.target.value)}
			            required
			        />
			    </Form.Group>
			    
			    { isActive ?
			    	<Button className="mb-3 btn btn-block" variant="primary" type="submit">
			            Submit
			    	</Button>
			    	:
			    	<Button disabled className="mb-3 btn btn-block" variant="secondary">
			            Submit
			    	</Button>
			    }
			    
			    <GoogleLogin 
			    	clientId="1069368278823-o46qggfgrnknd3mmhmulcmr0ds8ce7pq.apps.googleusercontent.com"
			    	buttonText="Login with Googol"
			    	onSuccess={ authenticateGoogleToken }
			    	onFailure={ authenticateGoogleToken }
			    	cookiePolicy={ 'single_host_origin' }
			    	className="w-100 text-center d-flex justify-content-center"
			    />
			</Form>
		</Container>
	);
};
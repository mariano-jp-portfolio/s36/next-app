import { useContext, useEffect } from 'react';
import UserContext from '../../UserContext';
import { useRouter } from 'next/router';

export default function index() {
	// router
	const router = useRouter();
	
   const { unsetUser } = useContext(UserContext);

   useEffect(() => {
      unsetUser();
      router.push('/login');
   }, []);
   
   return null
}